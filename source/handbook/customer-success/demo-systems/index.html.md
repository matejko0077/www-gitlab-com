---
layout: handbook-page-toc
title: "Demo Systems"
---

This handbook page at [about.gitlab.com/handbook/customer-success/demo-systems](https://about.gitlab.com/handbook/customer-success/demo-systems) is a mirror of the GitLab Demo Docs getting started page at [docs.gitlabdemo.com/getting-started](https://docs.gitlabdemo.com/getting-started) to allow the demo systems to be easily discoverable in the handbook. All other demo systems documentation is exclusively available on [docs.gitlabdemo.com](https://docs.gitlabdemo.com).

Please consider [docs.gitlabdemo.com](https://docs.gitlabdemo.com) to be the single source of truth ("SSOT") for all resources that use the `gitlabdemo.com` or `gitlabdemo.cloud` domain names.

## Overview of Demo Systems

The GitLab Demo Systems provide infrastructure for the GitLab Customer Success, Marketing, Sales, and Training teams to demonstrate GitLab features, value propositions, and workflows in a variety of asynchronous and live capacities.

### Demo Portal (Dashboard)

To get started with the GitLab Demo Cloud, you need to create an account on our Demo Portal at [gitlabdemo.com](https://gitlabdemo.com).

After you create your account, you can access the Demo Portal dashboard which provides access to the demo catalog and all of the sandbox environments that you have access to, such as the Demo Cloud GitLab instance and any available integrations.

#### Access Request

You **do not need to create an access request** issue to access the demo systems. Please follow the tutorial for [creating and accessing your account](https://docs.gitlabdemo.com/tutorials/getting-started/creating-accessing-your-account/). 

We have automated the provisioning of our systems with self-service registration on our demo portal. We currently allow registration for all GitLab team members with a `@gitlab.com` email address. This environment is designed for Customer Success team members, however all GitLab organization team members are welcome to use our environment.

We offer restricted access to specific demo environments using an invitation code redemption system that is used by customers and partners that are enrolled in some of our training classes or workshops.

### Getting Started Tutorials

We offer a variety of tutorials with step-by-step-instructions to help you get started using the demo systems.

After you have completed the applicable tutorials, we recommend that you export your projects that are in legacy environments or GitLab.com and import your example projects and repositories into your newly created group to start using the GitLab Demo Cloud.

Explore the tutorials at [docs.gitlabdemo.com/tutorials](https://docs.gitlabdemo.com/tutorials)

### Demo Catalog (Click-Through Demos)

The demo catalog provides multiple libraries of click-through demo content that allows you easily discover, learn about, and share each of the GitLab features and workflows. Each catalog library contains a variety of YouTube videos (from GitLab and GitLab Unfiltered), Google Slides presentations, and Markdown content.

The catalog is organized into libraries that allow different information architectures so that everyone can contribute into the library that makes the most sense for your use case.

Explore the catalog at [gitlabdemo.com/catalog](https://gitlabdemo.com/catalog)

#### Contributing to the Catalog

We offer self-service contributions when exploring our catalog as a user. You do not need to be an administrator to contribute content, however an administrator needs to approve contributed content before it is published and accessible to other users.

When viewing categories or scenario topics in the catalog, you will see a green **Contribute a Scenario** or **Contribute a Version** button. This provides a form for you to suggest additional topics or updated versions of content by provided a YouTube video ID or Google slides with helpful content. You will be able to preview a draft of your content after it has been submitted. Our administrators will review your suggestion and will follow up with you whether it is published or not.

To get involved with catalog contributions, please join the `#demo-systems-catalog` channel on Slack and introduce yourself and how you'd like to help.

## Sandbox Environments (Live Demos)

We offer different environments ranging from fully managed turn key environments to self-managed compute or containers. We recommended our shared demo cloud for most users unless you have use cases that require root access or specific architectural deployments.

Our goal is to make infrastructure provisioning and support easy and allow you to get started quickly and provide you as much admin rights as you need, while centrally managing our cloud provider usage and costs across the team.

#### Comparison of Environment Types

| Type of Environment      | Difficulty to Use | Support Provided | Access Provided                                       |
|--------------------------|-------------------|------------------|-------------------------------------------------------|
| Shared ("Demo Cloud")    | Easy to Moderate  | Dedicated Staff  | GitLab instance user and group (no root access)       |
| Container and VM Sandbox | Moderate to Hard  | Slack Community  | Root access to Linux instances and installed software |
| Compute Sandbox          | Moderate to Hard  | Slack Community  | AWS/GCP IAM user with dedicated VPC                   |

### Shared Environment ("Demo Cloud")

> This environment provides a direct replacement for our legacy demo systems that were deprecated in March 2020.

The demo systems that we call "GitLab Demo Cloud" provides a perpetual shared GitLab instance that is used for demos and collaborating with other team members with example projects that showcase the features and solutions that GitLab offers. The GitLab Demo Cloud is comparable to our hosted gitlab.com SaaS service, however it allows greater flexibility for demonstration and sandbox purposes without affecting our production environment.

The GitLab Demo Cloud provides you access to Ultimate license features with your own user account and an organizational group that you can use for creating projects and child groups. We also support integrations with Kubernetes, Jenkins, JIRA and other 3rd party integrations.

[Learn more about the Shared Environment ("Demo Cloud")](https://docs.gitlabdemo.com/environments/shared/)

### Container and VM Sandbox Environments

When you're experimenting with containers or virtual machine (VM) instances, it can be daunting and expensive to manage your own Kubernetes clusters or virtual private cloud (VPC).

We offer easy provisioning of ephemeral clusters, containers or VM instances for sandbox purposes that provide root-level access to GitLab instances and related resources that don't need a full environment deployed in our compute sandbox, AWS, or GCP.

[Learn more about the Container and VM Sandbox](https://docs.gitlabdemo.com/environments/container/)

### Compute Sandbox Environments

When you need access to cloud provider console to accomplish your goals, our compute sandbox provides you with access to a GCP and/or AWS account to deploy the compute resources, serverless or other managed services that you need.

You are responsible for all resources that you create and the demo systems team offers minimal to no support for the compute environments aside from access control and cost controls.

As a rule of thumb, this is a "do whatever you need to" environment, with a few "rules of the road" for cost management and security controls.

[Learn more about the Compute Sandbox](https://docs.gitlabdemo.com/environments/compute/)

## Need Help?

We have comprehensive documentation at [docs.gitlabdemo.com](https://docs.gitlabdemo.com). You can also explore our repositories at [gitlab.com/gitlab-com/customer-success/demo-systems](https://gitlab.com/gitlab-com/customer-success/demo-systems) (internal).

If you have issues, please post in `#demo-systems` on Slack and tag the `@demo-systems-admins` group for assistance.

We have additional `#demo-systems-*` channels in Slack for communities of interest or users that have access to specific systems, however we encourage you to start with `#demo-systems`.

You can also create issues for feature requests or reporting bugs in [Demo Feature Requests and Projects](https://gitlab.com/groups/gitlab-com/customer-success/demo-systems/demo-feature-requests/-/issues) (internal).

Please see our [issue board](https://gitlab.com/groups/gitlab-com/customer-success/demo-systems/demo-feature-requests/-/boards/1455569) (internal) to learn more about what we're working on next.

For users without a `@gitlab.com` account, please email `demo-systems-admin@gitlab.com` for assistance.

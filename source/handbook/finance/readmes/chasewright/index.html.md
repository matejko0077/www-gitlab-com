---
layout: markdown_page
title: "Chase Wright's README"
job: "Manager, FP&A"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Chase's README

 This page was inspired by the book [High Growth Handbook](http://growth.eladgil.com/book/the-role-of-the-ceo/insights-working-with-claire/).
 
My name is Chase Wright and I'm the Manager, Corporate FP&A at GitLab.
 
* [GitLab Handle](https://gitlab.com/wwright)
* [Team Page](/company/team/#wwright)
* [Chase's 1:1 Template](https://docs.google.com/document/d/1TAPGgJ3rREKskj8rgoTp_VSwu7tbkDxZ9Kd4cPWaEAc/edit#)
 
First of all, I’m really excited to be working with each of you and scaling GitLab together.

## OPERATING APPROACH

- I work best in structured conditions and dislike disorganization. I try to stay very organized. For me, things must be done sequentially and according to past successes. I will break the task down into smaller components and then into more efficient units. When I have organized the units into a methodical way of doing the task, I will expect everyone, including myself, to apply this process every time.

- High execute. Once I am on task, I forge ahead to reach the objective without looking back. What needs to be done next must be done immediately, with high standards, and on time. Even if the task is boring, I will remain focused until its done. 

- Bi-weekly or weekly 1:1s. We’ll try to keep the times consistent so you can plan. I’m a big fan of a 1:1 doc to track our agendas, actions, goals, and updates. I'm also a big fan of reducing meetings. If there is not an action item that comes away from a meeting, we don't need have one. 

- Weekly team meetings, as appropriate—I view these as both update to the team and decision-making/work review forums. I expect people to be prepared and to participate, even though we’ll have to manage video conferences and time zones.

- Quarterly planning sessions—it’s my hope we make these happen with strong pre-work and good follow-up afterward with our teams and partners (internal or external).

- Annual planning sessions-again, its my hope that we have strong pre-work going into the sessions but also realize operations do not stop. I respect that but also expect that we work together during these sessions to make GitLab the best planned company. 

- Speaking of 1:1s

  - 90 Day Plan-when you start, there will be a board of GitLab issues that we will work on together. This will help you understand the business, where I need help and what you will own. 

  - We’ll do a career session at some point in our first few months of working together—your history, why you’ve made choices you have made, what your ambitions are for the future, etc. These help me know where you are in terms of personal development interests and ambitions with respect to longer-term plans.

  - Personal goals—I believe in the two of us reviewing the top 3 personal goals you have each quarter or so (these are the things that you personally spend your time on, not your team plans, which I know you also spend time on…). We can discuss them each Q and then mark out a plan on how we make sure you get the time, space, and support to accomplish what you need. I do these every 3–6 months and will share mine with everyone, also.

- Your teams
  
  - Please add me to GitLab issues or correspondence that might be helpful for me to see as a way to understand the team and day-to-day work.

  - When necessary, please help me find out who is best to answer specific questions I may have about certain topics. I deep dive probably more than I should and sometimes there are better folks to answer questions than me. 

  - Finally, I look forward to personally meeting you and look forward to accomplishing our goals together. 

## MANAGEMENT STYLE

### Decision Making 

I live in the realm of thinking and acting simultaneously. Before proceeding, however, I like to have these questions answered:

- "What's the objective?"
- "Why are we doing this?"
- "What is the most efficient way of doing this?"

I am decisive and rely on common sense and logic, althought that can impair my ability to explore different ways. I take action items really seriously and I expect you to know what yours are, when they are due, and get them done. I don’t like chasing them but I do notice when things slip—it’s fine to renegotiate deadlines but I’ll be annoyed if it’s the day after the deadline….I dislike being caught last-minute with people working hard on something we could have gotten ahead of—please help anticipate big work efforts and let’s be in front of them together. Similarly, I want us to be ruthless in priorities while we are resource-constrained. I need you all sane…and me too. I live by deduction. 

### Hands-off

I’m not a micro-manager and I won’t sweat your details *unless* I think things are off track and if I do, I’ll tell you my concern and we can work together to make sure I understand and plan together on how to communicate better or right the situation. That said, when I am new to a project/ team I often get into the work alongside people so I can be a better leader—I will get involved in details and be more hands-on early on in a new initiative and just be warned on that. It’s how I will know how to help if you need me later.
I expect you are making decisions a lot without me and if you come to me I’ll usually put it back on you with, “What do you want to do?” or “What should you do?” and just help you decide. 

I also want to be kept informed on what you are working on that impact more than a few folks. I don't like being caught off guard on something that my team is working on, but I don't know about. Overcommunicating is better than not. 

### Data-driven

I like data, coding and analysis. I realize that it can be hard to find answers to hard questions, especially when using data. I also know the shortcomings of disorganized data and tight deadlines. Let's review objectively on what really matters, use data to get insight, then drive action. 95% of the time when researching a question using data, I will surface other business process inefficiencies or learn more about the buisness. Let's focus on the oringial question together, frame a hypothesis and note other business process inefficiencies along the way. 


### Frugal & Focused

I'm cheap, mostly self taught, and live debt free. What does this have to do with my management style? Hard work takes a while to pay off and trying to solve problems faster with additional expense is, more times than not, not the right answer.....says the finance guy. Let's work hard, stay focused on the end goal, and push distractions away. 

### Iterative 

Which leads me to being iterative. I want to see results, even if they are small. It lets me know that progress is being made and time is being focused on the problems.


### Big Picture

I try think where GitLab will be in x years and the most efficient way we can get there, but realize its not always a straight path (i.e. Covid-19). I go back to the questions under the **Decision Making** point above. If I work on the most important item today, what is it and how will it help GitLab get to where it is going faster? I sometimes over commit and get buried with extra tasks that take away from big picture items. If you see me doing this, tell me to stop :). Some problems seem more interesting at certain points in a time frame. 

### Customer oriented

I put this last because I think of my key leverage as more about scale than individual customer work, but I’m always interested in sales status, customer issues, customer stories, and meetings with users. I joined GitLab because of the product and was originally a customer. We should always do what is best for the customer. 

## COMMUNICATION

### 1:1s

Use 1:1s for items better discussed verbally and items that can wait for our weekly check-in. Email takes a *ton* of time and are inefficient, so use it wisely.
If we have a 1:1, its generally 2-4 times a month. 

### Email

I only use email when dealing with a customer, candidate, or vendor. I do not prefer it and think it is inefficient. Tasks or to-dos live in GitLab issues. Discussiosn live in 1:1s. Quick communications live in Slack. 

### Slack

If it’s urgent/impt/timely or super short feel free to Slack me any time. I prefer it always to be in a public channel but realize some questions are best in a private messeage. Do the best you can to fit the question into a public forum. If it’s a long topic and not time sensitive, maybe just wait for our 1:1.
Overall, I like more communication rather than less and I like to know what’s going on with you and your team and that helps me do a better job for you. I don’t view that as micromanagement but if you feel like I am too much in the weeds, please tell me. Finally, I don’t believe I will create a lot of email volume and I’ll be the first to recommend we do a quick in-person sync to resolve something versus a long email exchange. Or better yet, you can be the first to recommend it, and I’ll be the second.

I also like plans that are documented. I don’t care if it’s slides or docs or spreadsheets but I expect detailed work has been done when needed and if you have WIP or plans, I love to be included early and often in their development *but* I’ll generally only weigh in when asked or on final review, even if I have draft access.

### Feedback

I like it. I like to give it and I like to receive it, particularly constructive. We’re in this to get better together. We’ll have a quarterly official session but I’ll try to be timely when I observe or hear something and please do the same. I also like to know how and what your team is thinking and feeling and I will do skip levels, office hours, etc. Remember, whatever I hear or see, I have your back and I’ll tell you when I’m concerned. Anyone who vents to me about you is going to get my help to tell you directly.

### Management and people

I have a habit about choosing results over personal progress. I care about our time together at GitLab and want what is best for GitLab. However, I understand personal progress, learning, and **people** make up the company's success. Help me remember that I am here to mentor you, help you grow and get you to where you want to be. It's my responsibility to send the elevator back down. 

### Results

Let’s create meaningful OKRs that push GitLab to success. Measure measure measure :)

### Why So Serious?

Finally, I like to laugh and to have fun with the people I work with. It's a constructive way to vent also. 
 
Do you know something about me that belongs here? [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issue) 

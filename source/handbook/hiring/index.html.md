---
layout: handbook-page-toc
title: "Hiring"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hiring pages

- [Data Driven Recruiting](/handbook/hiring/data-driven-recruiting)
- [Diversity, Inclusion & Belonging  Recruiting Initiatives](/handbook/hiring/d-&-i-recruiting-initiatives)
- [Greenhouse](/handbook/hiring/greenhouse/)
- [Hiring Charts](/handbook/hiring/charts/)
- [Interviewing](/handbook/hiring/interviewing/)
- [Job families](/handbook/hiring/job-families/)
- [Job offers and post-interview processes](/handbook/hiring/offers/)
- [Preferred Companies to Recruit from](/handbook/hiring/preferred-companies/)
- [Principles](/handbook/hiring/principles/)
- [Recruiting Alignment](/handbook/hiring/recruiting-alignment/)
- [Recruiting Metrics Process](/handbook/hiring/metrics/)
- [Recruiting Process Framework](/handbook/hiring/recruiting-framework/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Sourcing](/handbook/hiring/sourcing/)
- [Vacancies](/handbook/hiring/vacancies/)

Potential applicants should refer to the [jobs FAQ page](/jobs/faq/).

## Related to hiring

- [Background checks](/handbook/people-group/code-of-conduct/#background-checks)
- [Benefits](/handbook/total-rewards/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Contracts](/handbook/contracts)
- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Onboarding](/handbook/general-onboarding)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)

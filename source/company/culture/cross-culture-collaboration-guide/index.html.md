---
layout: markdown_page
title: "Cross-Culture Collaboration Guide"
---

## On this page
{:.no_toc}

- TOC
{:toc}

As an organization with [67+ countries and regions](/company/team/) represented, it is ever-important for all of us to increase our cultural awareness, sensitivity, and understanding. This guide will help GitLab team members in effective and inclusive communication and cross-cultural collaboration globally; irrespective of culture or background. 

## Individual Culture vs. Company Culture

An important principle in any global organization is the distinction between our cultural background vs. the GitLab company culture. A critical aspect of our recruitment process is ensuring the people we hire are aligned with our [company values](/handbook/values/). 

Our diverse backgrounds help us live out our [Diversity, Inclusion & Belonging ](/handbook/values/#diversity--inclusion) value, while alignment with our company values allow us to collaborate as a _team_. While maintaining respect for each team member’s individual culture, GitLab culture and values should be what unites us. For many team members, some company values and the digital and [asynchronous](/handbook/communication/#internal-communication) way in which we work at GitLab may take some adjusting to during onboarding. We should support and encourage each other to [assume positive intent](/handbook/values/#assume-positive-intent) and live out our values daily!

## Country-Specific Cultural Dimensions

There has been much research done on how to understand the cultural dimensions of a country/region and the impact different dimensions have on communication and collaboration. Below, we have summarized the dimensions of one of the most well-known pioneer studies conducted by Geert Hofstede in his book `Cultures and Organizations: Software of the Mind` on cross-cultural groups and organizations. As a next iteration, GitLab will explore how we can customize this research to align with our values with an end goal of creating cultural dimensions tailored to our company to help foster understanding and communication.

_It is important to note that while the dimensions listed below can help us understand general orientations and trends in several countries based on research, these dimensions are not absolute. Each person is unique and thus the dimensions are relative._

### Hofstede’s 6 primary [cultural dimensions](https://hi.hofstede-insights.com/national-culture):

1. Power Distance Index (PDI)
1. Individualism vs. Collectivism (IDV)
1. Masculinity vs. Femininity (MAS)
1. Uncertainty Avoidance Index (UAI)
1. Long Term Orientation vs. Short Term Orientation (LTO)
1. Indulgence vs. Restraint (IVR)

Hofstede’s research has developed into a [cultural dimension comparison](https://www.hofstede-insights.com/country-comparison/) tool, where you are able to type in 70+ different countries for a comparative overview of the 6 cultural dimensions and what they mean.

## GitLab's Cultural Dimensions

Inspired by Hofstede's research above, we are working to develop cultural dimensions we should all be aware and consider when collaborating cross-culturally. [Open Issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/634)


### Stories from Team Members

We will be working to gather several stories shared with us by GitLab team members to highlight the importance of cultural awareness. [Open Issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/634)



